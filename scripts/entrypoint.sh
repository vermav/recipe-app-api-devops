#!/bin/sh

set -e
export PYTHONPATH=$PATH 
echo "*****"
echo $PATH
echo $PYTHONPATH
pwd
ls -lrt
env
echo "**FOUR***"
python manage.py collectstatic --noinput
python manage.py wait_for_db
python manage.py migrate
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi
